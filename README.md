# Displat Packer

This repository contains the [Packer](https://packer.io) code for creating the base images our project uses.

## Why Packer?

Packer allows us to preinstall dependencies ahead of time in an organised fashion, enabling faster and more secure deployments.

## Current images produced

After The project has reached MVP, we will support a lot more base images. Retention policy TBD, but LTS versions will be supported as long as their own support 
is valid.

| Platform | Ubuntu LTS 20.04 |
| --- | --- |
| DigitalOcean | 🚧 |

## How to use - WIP - THIS WARNING WILL BE REMOVED WHEN USABLE

**Note - for standard usage of the complete Displat stack, you should not need to use this repo. The images here will be created, hosted, and maintained
 by the project for public usage.**

Open `src` and run `gen.py`. This will generate a full set of valid permutations as individual Packer JSON files from the builders and provisioners in `src`.

Generation is done by reading `builds.toml`, which provides two things:
- A map of host providers to their available images paired with the builder file for said images
- A map of image purposes (ie Ubuntu Nomad Host Machine) to the set of provisioners for that purpose

`builds.toml` allows us to break up the components of each image end result into reusable JSON objects.

You can disable hosting providers and base images through the `--host=HOST` and `--base=BASE_IMAGE` flags. When generating, you can use `--no-defaults` to make
 those flags into opt-ins rather than opt-outs. The `base` flag is processed with Python Regex.

In `src`, `build.py` will use the generated Packer JSON files and create the images for them.
